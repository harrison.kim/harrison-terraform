terraform {
  backend "gcs" {
    bucket = "harrison-tf-state"
    prefix = "terraform/gcs"
  }
}
