locals {
  project_id           = "canvas-pathway-310407"
  location             = "us-east1"
  gcp_provider_version = "~> 3.63"
}
