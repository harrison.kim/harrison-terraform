resource "google_container_cluster" "harrison_terraform_test" {
  provider         = google-beta
  name             = "harrison-terraform-test"
  enable_autopilot = true
  location         = local.location
  project          = local.project_id
  addons_config {
    # istio not allowed for autopilot clusters
    istio_config {
      disabled = true
    }
  }
}
