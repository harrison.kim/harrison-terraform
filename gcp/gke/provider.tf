provider "google" {
  project = local.project_id
  region  = local.location
  version = "~> 3.63"
}

provider "google-beta" {
  project = local.project_id
  region  = local.location
  version = "~> 3.63"
}

terraform {
  backend "gcs" {
    bucket = "harrison-tf-state"
    prefix = "terraform/gke"
  }
}
