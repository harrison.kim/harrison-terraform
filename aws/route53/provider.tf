provider "aws" {
  region  = "us-east-1"
  version = "~> 3.36"
}

terraform {
  backend "s3" {
    bucket = "harrison-tf-state"
    key    = "terraform/route53"
    region = "us-east-1"
  }
}
