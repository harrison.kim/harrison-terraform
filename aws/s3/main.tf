locals {
  tags = {
    managed_by = "terraform"
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "harrisons-test-bucket"
  tags = merge({
    Name = "harrisons-test-bucket"
  }, local.tags)
}

resource "aws_s3_bucket" "example_bucket" {
  bucket = "harrisons-aws-example-for-kevin"
  tags = merge({
    Name  = "harrisons-aws-example-for-kevin",
    hello = "world"
  }, local.tags)
}

resource "aws_s3_bucket" "harrison_tf_state" {
  bucket        = "harrison-tf-state"
  acl           = "private"
  force_destroy = false
  tags = merge({
    Name = "harrison-tf-state"
  }, local.tags)
}
